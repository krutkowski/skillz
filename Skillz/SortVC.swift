
import UIKit

class SortVC: UIViewController {

	@IBOutlet var sortVisualisationView: SortVisualisationView!

	var sort: Sort!

	override func viewDidLoad() {
		super.viewDidLoad()

		var dataPoints = [Int]()
		for _ in 0..<20 {
			dataPoints.append(random() % 300)
		}

		let sth = QuickSort(items: dataPoints)
		sth.doStep()

		//		let dataPoints = [5, 1, 4, 2, 8]
		sortVisualisationView.startWithDataPoints(dataPoints)
		sortVisualisationView.animationTime = 0.2

		sort = QuickSort(items: dataPoints)
		sort.sortVisualisationDelegate = sortVisualisationView

		NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(SortVC.timerTick), userInfo: nil, repeats: true)

	}

	func timerTick() {
		sort.doStep()
	}

}
