
protocol SortVisualisationDelegate {

	func highlightItemsAtIndexes(indexes: Set<Int>)
	func unhighlightItemsAtIndexes(indexes: Set<Int>)
	func swapItemsAtIndex1(index1 index1: Int, index2: Int)

}

class Sort {

	var items: [Int]
	var sortVisualisationDelegate: SortVisualisationDelegate?
	var finished = false

	func doStep() {
		preconditionFailure()
	}

	init(items: [Int]) {
		// TODO: check if more than 2 items
		self.items = items
	}

}