
class QuickSort: Sort {

	override func doStep() {
		innerQuicksort(0, to: <#T##Int#>, completion: <#T##(Void -> Void)##(Void -> Void)##Void -> Void#>)
		innerQuicksort(0, to: items.count - 1, )
	}

	func innerQuicksort(from: Int, to: Int, completion: (Void -> Void)) {
		if from < to {
			let p = partition(from, to: to)
			innerQuicksort(from, to: p - 1)
			innerQuicksort(p + 1, to: to)
		}
	}

	func partition(from: Int, to: Int) -> Int {
		let pivot = items[to]
		var i = from

		for j in from..<to {
			if items[j] <= pivot {
				if i != j {
					swap(&items[i], &items[j])
				}
				i += 1
			}
		}

		if i != to {
			swap(&items[i], &items[to])
		}

		return i
	}
}

