
class BubbleSort: Sort {

	private var currentIndex = 0
	private var didSwap = false
	private var didHighlightCurrentPair = false
	private var didCheckCurrentPair = false
	private var didUnhighlightCurrentPair = false

	override func doStep() {
		if finished {
			return
		}

		if !didHighlightCurrentPair {
			var set = Set<Int>()
			set.insert(currentIndex)
			set.insert(currentIndex + 1)
			sortVisualisationDelegate?.highlightItemsAtIndexes(set)

			didHighlightCurrentPair = true
		} else if !didCheckCurrentPair {
			didCheckCurrentPair = true

			if items[currentIndex] > items[currentIndex + 1] {
				let firstItem = items[currentIndex]
				let secondItem = items[currentIndex + 1]
				items[currentIndex] = secondItem
				items[currentIndex + 1] = firstItem

				sortVisualisationDelegate?.swapItemsAtIndex1(index1: currentIndex, index2: currentIndex + 1)

				didSwap = true
			} else {
				doStep()
			}
		} else if !didUnhighlightCurrentPair {
			var set = Set<Int>()
			set.insert(currentIndex)
			set.insert(currentIndex + 1)
			sortVisualisationDelegate?.unhighlightItemsAtIndexes(set)

			//didUnhighlightCurrentPair = true
			didHighlightCurrentPair = false
			didCheckCurrentPair = false

			if currentIndex == items.count - 2 {
				if !didSwap {
					finished = true
				} else {
					didSwap = false
					currentIndex = 0
				}
			} else {
				currentIndex += 1
			}
		}
	}

}
