
import UIKit

class SortVisualisationView: UIView, SortVisualisationDelegate {

	private var firstBarPadding: CGFloat {
		if dataPoints.count > 40 {
			return 0
		} else {
			return 1
		}
	}
	private var barWidth: CGFloat {
		if dataPoints.count > 40 {
			return bounds.width / CGFloat(dataPoints.count)
		} else {
			return (bounds.width - 2 - CGFloat(dataPoints.count - 1)) / CGFloat(dataPoints.count)
		}
	}
	private var barSpacing: CGFloat {
		if dataPoints.count > 40 {
			return 0
		} else {
			return 1
		}
	}
	private var bottomSpacing: CGFloat = 10
	private var heightPerUnit: CGFloat {
		return (bounds.height - bottomSpacing) / CGFloat(dataPoints.maxElement()!)
	}
	private var dataPoints = [Int]()

	lazy private var underline: UIView = {
		let underline = UIView()
		underline.backgroundColor = UIColor.darkGrayColor()
		self.addSubview(underline)

		return underline
	}()

	var animationTime: NSTimeInterval = 0.3

	func startWithDataPoints(dataPoints: [Int]) {
		self.dataPoints = dataPoints
		redrawBars()
	}


	override var bounds: CGRect {
		didSet {
			redrawBars()
		}
	}

	func highlightItemsAtIndexes(indexes: Set<Int>) {
		var minX: CGFloat? = nil
		var maxX: CGFloat? = nil
		for index in indexes {
			let view = viewWithTag(index)!

			if minX != nil {
				if minX! > view.frame.origin.x {
					minX = view.frame.origin.x
				}
			} else {
				minX = view.frame.origin.x
			}

			if maxX != nil {
				if maxX! < view.frame.maxX {
					maxX = view.frame.maxX
				}
			} else {
				maxX = view.frame.maxX
			}
		}

		underline.alpha = 1

		if let maxX = maxX, minX = minX {
			UIView.animateWithDuration(animationTime) { () -> Void in
				self.underline.frame = CGRect(x: minX, y: self.bounds.height - self.bottomSpacing + 1, width: maxX - minX, height: 2)
			}

		}
	}

	func unhighlightItemsAtIndexes(indexes: Set<Int>) {
		for index in indexes {
			viewWithTag(index)!.backgroundColor = UIColor.lightGrayColor()
		}
	}

	func swapItemsAtIndex1(index1 index1: Int, index2: Int) {
		let index1Value = dataPoints[index1]
		dataPoints[index1] = dataPoints[index2]
		dataPoints[index2] = index1Value

		let bar1 = viewWithTag(index1)!
		let bar2 = viewWithTag(index2)!
		bar1.tag = index2
		bar2.tag = index1

		var frame1 = bar1.frame
		var frame2 = bar2.frame

		frame1.origin.x = bar2.frame.origin.x
		frame2.origin.x = bar1.frame.origin.x
		UIView.animateWithDuration(animationTime) { () -> Void in
			bar1.frame = frame1
			bar2.frame = frame2
		}
	}

	private func redrawBars() {
		tag = -1
		for subview in subviews {
			subview.removeFromSuperview()
		}

		for (index, dataPoint) in dataPoints.enumerate() {
			let x = firstBarPadding + (barWidth + barSpacing) * CGFloat(index)
			let height = heightPerUnit * CGFloat(dataPoint)
			let y = bounds.height - height - bottomSpacing

			let bar = UIView(frame: CGRect(x: x, y: y, width: barWidth, height: height))
			bar.tag = index
			bar.backgroundColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.5) //index % 2 == 0 ? UIColor.lightGrayColor() : UIColor.darkGrayColor()
			addSubview(bar)
		}
	}
}
